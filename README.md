Minds E2E Suite
===========
[![Gitlab CI Status](https://www.gitlab.com/minds/e2e/badges/master/pipeline.svg)](https://www.gitlab.com/minds/e2e)

Integration tests for the [front](https://github.com/minds/minds) and [engine](https://github.com/minds/engine) repositories to make use of.

### Running

`npm run e2e --config baseUrl=http://localhost --env username=minds,password=Pa$$w0rd`

## License
[AGPLv3](https://www.minds.org/docs/license.html). Please see the license file of each repository.

___Copyright Minds 2019___
